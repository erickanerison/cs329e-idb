from flask import Flask, render_template
from models import app, db, State, Nation, Center
from flask import request
from sqlalchemy import desc

@app.route('/', methods=['GET'])
def splash():
    return render_template('splash.html')

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/centers/', methods=['POST', "GET"])
# This will go to the page with tables and images of the centers we have
def centers():
    states = db.session.query(State).all()
    nations = db.session.query(Nation).all()
    centers = db.session.query(Center).all()
    value = request.form.get('sorting')
    if value == "11" or value == "0":
        centers = db.session.query(Center).all()
    elif value == "12":
        centers = centers[::-1]
    elif value == "21":
        centers = db.session.query(Center).order_by(Center.deported_percentage)
    elif value == "22":
        centers = db.session.query(Center).order_by(desc(Center.deported_percentage))
    return render_template('centers.html', centers = centers, states = states, nations = nations)

@app.route('/centers/<int:center_id>')
# This will go to the page for every individual center
def specificCenter(center_id):
    specific_Center = db.session.query(Center).get(center_id)
    return render_template('specificCenter.html', center_id = center_id, center = specific_Center)

@app.route('/states', methods=['POST', "GET"])
# This will go to the page with tables and images of the states we have
def states():
    states = db.session.query(State).all()
    nations = db.session.query(Nation).all()
    centers = db.session.query(Center).all()
    value = request.form.get('sorting')
    if value == "11" or value == "0":
        states = db.session.query(State).all()
    elif value == "12":
        states = states[::-1]
    elif value == "21":
        states = db.session.query(State).order_by(State.deportation_percentage)
    elif value == "22":
        states = db.session.query(State).order_by(desc(State.deportation_percentage))
    return render_template('states.html', states = states, nations = nations, centers = centers)

@app.route('/states/<int:state_id>')
# This will go to the page for every individual state
def specificState(state_id):
    specific_State = db.session.query(State).get(state_id)
    return render_template('specificState.html', state_id = state_id, state = specific_State)

@app.route('/nations/', methods=['POST', "GET"])
# This will go to the page with tables and images of the nations we have
def nations():
    nations = db.session.query(Nation).all()
    states = db.session.query(State).all()
    value = request.form.get('sorting')
    if value == "11" or value == "0":
        nations = db.session.query(Nation).all()
    elif value == "12":
        nations = nations[::-1]
    elif value == "21":
        nations = db.session.query(Nation).order_by(Nation.remitances)
    elif value == "22":
        nations = db.session.query(Nation).order_by(desc(Nation.remitances))
    return render_template('nations.html', nations = nations, states = states)

@app.route('/nations/<int:nation_id>')
# This will go to the page for every individual nation
def specificNation(nation_id):
    specific_Nation = db.session.query(Nation).get(nation_id)
    return render_template('specificNation.html', nation_id = nation_id, nation = specific_Nation)

if __name__ == "__main__":
    app.run(debug=True)