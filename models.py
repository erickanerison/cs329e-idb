from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import json

app = Flask(__name__)
#app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///dbdir/test.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


class Center(db.Model):
    __tablename__ = "centers"
    center_id = db.Column(db.Integer, primary_key = True, nullable = False)

    name = db.Column(db.String)
    total_detained = db.Column(db.Integer)
    num_departed = db.Column(db.Integer)
    city = db.Column(db.String)
    state = db.Column(db.String)
    type = db.Column(db.String)
    highest_nationality_denomination = db.Column(db.String)
    highest_nationality_percentage = db.Column(db.Float)
    deported_percentage = db.Column(db.Float)
    image_link = db.Column(db.String)
    map_link = db.Column(db.String)

    def __init__(self, center_id, name, total_detained, num_departed, city, state, type, highest_nationality_denomination, highest_nationality_percentage, deported_percentage, image_link, map_link):
        self.center_id = center_id
        self.name = name
        self.total_detained = total_detained
        self.num_departed = num_departed
        self.city = city
        self.state = state
        self.type = type
        self.highest_nationality_denomination = highest_nationality_denomination
        self.highest_nationality_percentage = highest_nationality_percentage
        self.deported_percentage = deported_percentage
        self.image_link = image_link
        self.map_link = map_link

class State(db.Model):
    __tablename__ = "states"
    state_id = db.Column(db.Integer, primary_key=True, nullable = False)

    name = db.Column(db.String)
    homicide_rate = db.Column(db.Float)
    gdp = db.Column(db.Integer)
    number_of_centers = db.Column(db.Integer)
    number_of_incarcerations = db.Column(db.Integer)
    number_of_departures = db.Column(db.Integer)
    highest_nationality_denomination = db.Column(db.String)
    highest_nationality_percentage =  db.Column(db.Float)
    center_with_highest_population = db.Column(db.String)
    center_data = db.Column(db.Float)
    deportation_percentage = db.Column(db.Float)
    image_link = db.Column(db.String)

    def __init__(self, state_id, name, homicide_rate, gdp, number_of_centers, number_of_incarcerations, number_of_departures, highest_nationality_denomination, highest_nationality_percentage, center_with_highest_population, center_data, deportation_percentage, image_link):
        self.state_id = state_id
        self.name = name
        self.homicide_rate = homicide_rate
        self.gdp = gdp
        self.number_of_centers = number_of_centers
        self.number_of_incarcerations = number_of_incarcerations
        self.number_of_departures = number_of_departures
        self.highest_nationality_denomination = highest_nationality_denomination
        self.highest_nationality_percentage = highest_nationality_percentage
        self.center_with_highest_population = center_with_highest_population
        self.center_data = center_data
        self.deportation_percentage = deportation_percentage
        self.image_link = image_link

class Nation(db.Model):
    __tablename__ = "nations"
    nation_id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String)
    state_of_highest_emigration = db.Column(db.String)
    corresponding_center_with_highest_percent = db.Column(db.String)
    gdp = db.Column(db.Float)
    homicide_rate = db.Column(db.Float)
    remitances = db.Column(db.Integer)
    img_link = db.Column(db.String)

    def __init__(self, nation_id, name, state_of_highest_emigration, corresponding_center_with_highest_percent, gdp, homicide_rate, remitances, img_link):
        self.nation_id = nation_id
        self.name = name
        self.state_of_highest_emigration = state_of_highest_emigration
        self.corresponding_center_with_highest_percent = corresponding_center_with_highest_percent
        self.gdp = gdp
        self.homicide_rate = homicide_rate
        self.remitances = format(remitances, ",d")
        self.img_link = img_link


def readCenter():
    with open("trunc_centers.json", "r") as file:
        center_data = json.load(file)

    count = 0
    for key in center_data:
        center_id = count
        name = key['name']
        total_detained = key['total_detained']
        num_departed = key['num_departed']
        city = key['city']
        state = key['state']
        type = key['type']
        highest_nationality_denomination = key['highest_nationality_denomination']
        highest_nationality_percentage = key['highest_nationality_percentage']
        deported_percentage = key['deported_percentage']
        image_link = key['image_link']
        map_link = key['map_link']
        count += 1
        singleCenter = Center(center_id, name, total_detained, num_departed, city, state, type, highest_nationality_denomination, highest_nationality_percentage, deported_percentage, image_link, map_link)
        db.session.add(singleCenter)
        db.session.commit()

def readState():
    with open("trunc_states.json", "r") as file:
        center_data = json.load(file)

    count = 1000
    for key in center_data:
        state_id = count
        name = key['name']
        homicide_rate = key['homicide_rate']
        gdp = key['gdp']
        number_of_centers = key['number_of_centers']
        number_of_incarcerations = key['number_of_incarcerations']
        number_of_departures = key['number_of_departures']
        highest_nationality_denomination = key['highest_nationality_denomination']
        highest_nationality_percentage = key['highest_nationality_percentage']
        center_with_highest_population = key['center_with_highest_population']
        center_data = key['center_data']
        deportation_percentage = key['deportation_percentage']
        image_link = key['image_link']
        count += 1
        singleState = State(state_id, name, homicide_rate, gdp, number_of_centers, number_of_incarcerations, number_of_departures, highest_nationality_denomination, highest_nationality_percentage, center_with_highest_population, center_data, deportation_percentage, image_link)
        db.session.add(singleState)
        db.session.commit()

def readNation():
    with open("trunc_nations.json", "r") as file:
        center_data = json.load(file)

    count = 2000
    for key in center_data:
        nation_id = count
        name = key['name']
        state_of_highest_emigration = key['state_of_highest_emigration']
        corresponding_center_with_highest_percent = key['corresponding_center_with_highest_percent']
        gdp = key['gdp']
        homicide_rate = key['homicide_rate']
        remitances = key['remitances']
        img_link = key['img_link']
        count += 1
        singleNation = Nation(nation_id, name, state_of_highest_emigration, corresponding_center_with_highest_percent, gdp, homicide_rate, remitances, img_link)
        db.session.add(singleNation)
        db.session.commit()


db.create_all()
db.session.commit()
readCenter()
readState()
readNation()